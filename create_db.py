import argparse
import sqlite3
import pickle
import zlib
from sqlite3 import Error

PREFIX_LEN=6
BULKINSERTLIMIT=50000

def create_connection(db_file):
    """ create a database connection to a SQLite database """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        print(sqlite3.version)
    except Error as e:
        print(e)
    return conn

def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:        
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)

def create_db_schema():
    """ create the db schema, tables...etc"""
    db_filename='hibpntlm.db'
    sql_create_hash_table = """ CREATE TABLE IF NOT EXISTS ntlmhash (                                                                    
                                        prefix integer PRIMARY KEY,
                                        suffixes blob NOT NULL                                                                                                            
                                    ); """

    # create a database connection
    conn = create_connection(db_filename)

    # create tables
    if conn is not None:
        # create hash table
        create_table(conn, sql_create_hash_table)  
    else:
        print("Error! cannot create the database connection.")
    return conn


def check_for_current_db():
    return False

def insert_bulkdata(db_conn,bulkdata):    
    cur = db_conn.cursor()        
    suffixsql = f'INSERT INTO ntlmhash(prefix,suffixes) VALUES(?,?)'    
    cur.executemany(suffixsql, bulkdata)
    db_conn.commit()
    print(f' [+] Inserting {len(bulkdata)}')
    return cur.lastrowid

def compress_record(prefix, suffix_freqdata):
    serialized_d = pickle.dumps(suffix_freqdata)
    compress_d=zlib.compress(serialized_d)
    return (prefix, compress_d)

def parse_hibp_file(db_conn, hibpinputfile):
    print(f' [+] Parsing HIBP file: {hibpinputfile}')
    insert_buffer=[]
    suffix_freq_buffer={}
    last_prefix=None
    with open(hibpinputfile) as hibpfile: 
        count=0           
        while True:
            count+=1            
            line = hibpfile.readline()
            if not line:                
                if len(suffix_freq_buffer) > 0:
                    insert_buffer.append(compress_record(last_prefix,suffix_freq_buffer))
                if len(insert_buffer) > 0:
                    insert_bulkdata(db_conn,insert_buffer)
                break
            ntlmhash, freq = line.split(':')
            freq = int(freq,10)
            current_prefix = int(ntlmhash[0:PREFIX_LEN],16)            
            suffix = int(ntlmhash[PREFIX_LEN:],16)             
            if last_prefix is None:
                last_prefix=current_prefix             
            if current_prefix != last_prefix: 
                insert_buffer.append(compress_record(last_prefix,suffix_freq_buffer)) 
                if count > BULKINSERTLIMIT:
                    insert_bulkdata(db_conn,insert_buffer)
                    insert_buffer=[]
                    count=0
                suffix_freq_buffer={}              
            suffix_freq_buffer[suffix] = freq
            last_prefix=current_prefix
    db_conn.close()


def main(hibpinputfile):
    exists=check_for_current_db()
    if exists:
        print(f'[i] Database already exists')
    else:
        print(f' [+] Creating db file')
        conn=create_db_schema()
        parse_hibp_file(conn,hibpinputfile)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
                            description = 'Parses a HIBP ntlm hash file and creates the lookup db')
    parser.add_argument('-i',
                        '--hibpfile',
                        help='path to the HIBP ntlm lookup file')
    args=parser.parse_args()
    if not args.hibpfile:
        parser.error('Please specify the HIBP file path')

    main(args.hibpfile)
    input("Press Enter to continue...")