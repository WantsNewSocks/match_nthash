#!/usr/bin/python3
import os
import argparse
import collections
import check_dbmatch

PREFIX_LEN=6

def parse_nt_hashes(ntlmpath):
    adnthashes=[]
    with open(ntlmpath) as ntlmfp:
        lines=ntlmfp.readlines()
    for line in lines:
        hashobj={}
        s=line.split(':')
        hashobj['user']=s[0]
        hashobj['uuid']=s[1]
        hashobj['lmhash']=s[2].lower()
        hashobj['nthash']=s[3].lower()
        adnthashes.append(hashobj)
    return adnthashes

def check_lmhashes(hashobjs):
    print('Checking LM hashes...')
    if any(d['lmhash']!='AAD3B435B51404EEAAD3B435B51404EE' for d in hashobjs):
        print(f'\t[+] Non null LM hashes found')

def check_nt_duplicates(hashobjs):
    print('Checking NT duplicates...')
    nthashes=[d['nthash'] for d in hashobjs]
    counts=collections.Counter(nthashes)
    for nthash, cnts in list(counts.items()):
        if cnts < 2:
            del counts[nthash]
    for nthash, cnts in list(counts.items()):
        print(f'\tNT Hash {nthash} appears {cnts} times')

def main(ntlmpath):
    print(f'NTLM path {ntlmpath}')
    
    # create the AD NT hash, converted to lowercase
    adnthashes=parse_nt_hashes(ntlmpath)
    
    # check for non null lm hashes
    check_lmhashes(adnthashes)
    
    # check for duplicate nt hashes
    check_nt_duplicates(adnthashes)

    # create empty match object
    matched_results=[]

    # Use SQLLite3 db for optamised search
    print('Searching for HIBP matches ...')
    for obj in adnthashes:
        occurs=check_dbmatch.check_hash(obj['nthash'])
        if occurs > 0:
            print(f'\tMatch found for user {obj["user"]} with {obj["nthash"]}')
            obj['matchfreq']=occurs
            matched_results.append(obj)
    
    if len(matched_results) > 0:
        print(f'\n [+] {len(matched_results)} NT hash matches found in HIBP:')
        for match in matched_results:
            print(f'\tUser: {match["user"]}' + 
                  f' with NT Hash {match["nthash"]}' +
                  f' occurs {match["matchfreq"]}')
    else:
        print('\n [-] No matches found!')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
                            description = 'Match NTLM Hashes from HIBP tool')
    parser.add_argument('-n',
                        '--ntlmfile',
                        help='path to the ntlm hash file to be checked')
    args=parser.parse_args()
    if not args.ntlmfile:
        parser.error('Please specify the NTLM file path')

    main(args.ntlmfile)


