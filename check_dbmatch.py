import argparse
import sqlite3
import pickle
import zlib
from sqlite3 import Error

PREFIX_LEN=6
BULKINSERTLIMIT=50000

def create_connection(db_file):
    """ create a database connection to a SQLite database """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)
    return conn

def decompress_record(record):
    decompress_d=zlib.decompress(record[1])
    suffixdata = pickle.loads(decompress_d)
    return (record[0], suffixdata)

def lookup_prefix(prefix):
    """ Using SQLLite3 DB lookup prefix, will return suffix compressed blob"""
    db_filename='hibpntlm.db'
    sql_select_prefix = """Select * from ntlmhash Where prefix = ?"""

    # create a database connection
    conn = create_connection(db_filename)
    
    # perform query
    cur = conn.cursor()
    cur.execute(sql_select_prefix, (prefix,))

    # Process row result
    rows = cur.fetchall()    
    if len(rows) == 0:
        return (prefix, None)
    for row in rows:
        prefix, suffixes = decompress_record(row)
    return (prefix, suffixes)
    

def check_hash(ntlmhash):
    prefix = int(ntlmhash[0:PREFIX_LEN],16)
    suffix = int(ntlmhash[PREFIX_LEN:],16)
    prefix_match_data=lookup_prefix(prefix)    
    if prefix_match_data[1] is None:
        return 0
    else:
        # check suffixes for true match (not just prefix hit)
        if suffix in prefix_match_data[1]:
            # we have a suffix match
            return prefix_match_data[1][suffix]
    return 0
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
                            description = 'Looksup a NTLM within the Compressed SQLLite3 HIBP db')
    parser.add_argument('-n',
                        '--hash',
                        help='NTLM hash to lookup')
    args=parser.parse_args()
    if not args.hash:
        parser.error('Please specify a NTLM hash to lookup')

    occurs=check_hash(args.hash)
    if occurs > 0:
        print(f'{args.hash} found in HIBP with frequency {occurs}')
    else:
        print(f'{args.hash} not found in HIBP')