#!/usr/bin/python3
import os
import argparse
import collections

from collections import Counter

PREFIX_LEN=6

def parse_nt_hashes(ntlmpath):
    adnthashes=[]
    with open(ntlmpath) as ntlmfp:
        lines=ntlmfp.readlines()
    for line in lines:
        hashobj={}
        s=line.split(':')
        hashobj['user']=s[0]
        hashobj['uuid']=s[1]
        hashobj['lmhash']=s[2].lower()
        hashobj['nthash']=s[3].lower()
        if len(s) > 6:
            hashobj['status']=s[6].lower()         
        adnthashes.append(hashobj)
    return adnthashes

def check_lmhashes(hashobjs):
    print('Checking LM hashes...')
    if any(d['lmhash']!='AAD3B435B51404EEAAD3B435B51404EE' for d in hashobjs):
        print(f'\t[+] Non null LM hashes found')

def check_nt_duplicates(hashobjs):
    print('Checking NT duplicates...')
    nthashes=[d['nthash'] for d in hashobjs]
    counts=collections.Counter(nthashes)
    print(f'There are {len(counts)} unique nt hashes')
    for nthash, cnts in list(counts.items()):
        if cnts < 2:
            del counts[nthash]
    for nthash, cnts in list(counts.items()):
        print(f'\tNT Hash {nthash} appears {cnts} times')

def main(ntlmpath, potfilepath):
    print(f'NTLM path {ntlmpath}')
    
    # create the AD NT hash, converted to lowercase
    adnthashes=parse_nt_hashes(ntlmpath)
    
    # check for non null lm hashes
    check_lmhashes(adnthashes)
    
    # check for duplicate nt hashes
    check_nt_duplicates(adnthashes)

    # create empty match object
    matched_results=[]    

    # analysis potfile vs nthashes
    with open(potfilepath) as potfp:
        lines=potfp.readlines()
    potobjs=[]
    for line in lines:
        potobj={}
        s=line.split(':')
        potobj['nthash']=s[0]
        potobj['pass']=s[1]
        potobjs.append(potobj)

    print(f'Checking potfile')
    totalcnt=0
    for potobj in potobjs:
        pothash=potobj['nthash']
        potpass=potobj['pass']
        potcnt=0  
        print(' -----   -----')      
        print(f'\n{pothash}:{potpass.strip()}')
        for obj in adnthashes:
            nthash=obj['nthash']
            userstat=obj['status']
            if nthash == pothash:
                user=obj['user']
                print(f'\tFound {user} for hash {nthash} status {userstat}')
                potcnt+=1
                totalcnt+=1
        if potcnt >0:
            nthash=obj['nthash']
            print(f'Hash {nthash} found {potcnt} times')
            print(' -----   -----')      
        else:
            print('No matches')
            print(' -----   -----')      
    
    print(f'Total matches with pot file {totalcnt}')
              
    
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
                            description = 'Match NTLM Hashes from HIBP tool')
    parser.add_argument('-n',
                        '--ntlmfile',
                        help='path to the ntlm hash file to be checked')
    parser.add_argument('-p',
                        '--potfile',
                        help='path to the pot file to be checked')                        
    args=parser.parse_args()
    if not args.ntlmfile:
        parser.error('Please specify the NTLM file path')
    if not args.potfile:
        parser.error('Please specify the hashcat pot file path')

    main(args.ntlmfile, args.potfile)


