# Match NT Hash

## Getting NT hashes from AD using `DCSync` 

Using `Impacket` `DCSync` method to dump hashes:

```bash
/secretsdump.py -just-dc-ntlm  '<user>:<password>@<ip>' | grep ":::" > nthashes.txt
```

## Have I been Pwned NT Hashes
Troy Hunt supplies a large collection of hashes which can be downloaded:

```bash
wget https://downloads.pwnedpasswords.com/passwords/pwned-passwords-ntlm-ordered-by-count-v6.7z
```

## Usage
1) To build the `SQLLite3` database:
```
./create_db.py -i <hibp pwned nt hashes>
```

2) To perform Password / hash check
```bash
./match_ntlmhashes.py -n <nthash file>
```
